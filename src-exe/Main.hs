module Main where

import HtmlGen.PageGen

import System.Console.GetOpt
import System.Environment (getArgs)

data Flag
  = Sitemap
  deriving Show

options :: [OptDescr Flag]
options =
  [ Option ['s'] ["sitemap"] (NoArg Sitemap)         "Generate a sitemap.xml in addition to the pages."
  ]

compilerOpts :: [String] -> IO ([Flag], [String])
compilerOpts argv = 
  case getOpt Permute options argv of
    (o,n,[]  ) -> return (o, n)
    (_,_,errs) -> ioError (userError (concat errs ++ usageInfo header options))
  where header = "Usage: html-generator [OPTION...] files..."

main :: IO ()
main = do
  args <- getArgs
  (opts, files) <- compilerOpts args
  case opts of
    [] -> genPages files
    _  -> genPagesSitemap files

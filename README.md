# html-generator

Generates HTML and sitemaps from a Haskell webpage
description file.

## Describing pages

The data structure is contained within `HtmlGen.Html`.
I would recommend starting off using `newTag "tag"`
and then appending content to it, but you may also
use `tag`, `mkTag`, `emptyTag`, and `text` to
directly build Html.

There are five main operators in `HtmlGen.Html`:
* `myHtml %% ("attributeName", "value")` will append
an attribute to the `myHtml` tag.
* `myHtml %> myTag` will append `myTag` to the body
of `myHtml`.
* `myHtml %>> "Hello, world!"` will append
`Hello, world!` to the body of `myHtml`.
* `myHtml %!> tags` will append all of the elements in `tags`
to the body of `myHtml` sequentially. This words on any
foldable data structure, e.g. lists or Maybe.

There are also many builtin tags and other utilities
in `HtmlGen.Tags`, so I would recommend going through
that file.

## Generating HTML

All generated HTML is valid XHTML.

You can print your structure using `HtmlGen.Print`,
in particular `printHtml` to include `<!DOCTYPE html>`
and `printHtmlTree` to exclude it.

You may also store each page in its own individual
file, in a module named `Page` and the root HTML
in `page :: Html`. Haskell code may be used to
dynamically generate pages as desired through this
method.

```haskell
module Page where

import HtmlGen.Html
import HtmlGen.Tags
import HtmlGen.Sitemap

sitemap :: Sitemap
sitemap = baseSitemap "https://example.com/lorem-ipsum.xhtml"
  `withLastMod` "2018-05-05"

page :: Html
page = html %> (body %> (p %>> "Hello, World!"))
```

Once you have written your page, running
`stack exec html-generator path/to/file.hs` will write
the printed result to a file `path/to/file.xhtml`.
`stack exec` must be used or any imports (including
`HtmlGen`!) used in `file.hs` will not be on the path.

## Generating sitemaps

If you pass `--sitemap` or simply `-s` to `html-generator`,
it will generate a sitemap file `./sitemap.xml`. For example,
`stack exec html-generator -- --sitemap files...`.

To do this, you must specify the sitemap entry in every
page file as `sitemap :: Sitemap`. See the above example
for what that looks like in-context.

The tools for specifying sitemaps are pretty straightforward.
For more information on the valid sitemap options, see
[https://www.sitemaps.org/protocol.html](sitemaps.org).

```haskell
-- | Takes a URL and makes a sitemap entry from it.
baseSitemap :: String -> Sitemap
-- | Sets the relevant sitemap option to the given value.
withLastMod,withChangeFreq,withPriority :: Sitemap -> String -> Sitemap
```

You may also manually generate your own sitemap.xml using
`HtmlGen.Sitemap.printSitemap`.

## Using templates

Since Haskell is a proper programming language, you
can get a whole lot of code reuse via templates.
Any library containing `Html` may be used as for
templates. For example, see
[my website](https://github.com/jammar/jtmar.me/tree/master/templates/).

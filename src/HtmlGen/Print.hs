module HtmlGen.Print (printHtmlTree, printHtml) where

import Data.List (intersperse, null)
import HtmlGen.Html hiding (tag, emptyTag)

-- | Prepends the first element to the list only if the list is not empty.
prependIfNotEmpty :: [a] -> [a] -> [a]
prependIfNotEmpty _ [] = []
prependIfNotEmpty xs ys = xs ++ ys

-- | Prints an attribute key/value pair.
attribute :: Attribute -> String
attribute (key, value) = key ++ "=\"" ++ value ++ "\""

-- | Prints a list of attributes to a string.
attributes :: Attributes -> String
attributes = prependIfNotEmpty " " . unwords . map attribute

-- | Prints an open tag with attributes.
openTag :: String -> Attributes -> String
openTag name attrs = "<" ++ name ++ attributes attrs ++ ">"

-- | Prints a close tag.
closeTag :: String -> String
closeTag name = "</" ++ name ++ ">"

-- | Prints a self-closing tag (i.e. one with no body).
clopenTag :: String -> Attributes -> String
clopenTag name attrs = "<" ++ name ++ attributes attrs ++ " />"

-- | Prints a tag with no body.
emptyTag :: String -> Attributes -> String
emptyTag = clopenTag

-- | Prints a tag in proper XML format.
tag :: String -> Attributes -> String -> String
tag name attrs "" = emptyTag name attrs
tag name attrs body = openTag name attrs ++ body ++ closeTag name

-- | Prints an HTML structure in XHTML format.
printHtmlTree :: Html -> String
printHtmlTree (Tag name attrs body) = tag name attrs (concatMap printHtmlTree body)
printHtmlTree (Text txt) = txt

-- | Prints an HTML structure in XHTML format, including a !DOCTYPE declaration.
printHtml :: Html -> String
printHtml html = "<!DOCTYPE html>\n" ++ printHtmlTree html

{-# LANGUAGE GADTs #-}

module HtmlGen.Html ( Html (Tag, Text), Attribute, Attributes
                    , tag, emptyTag, mkTag, newTag, text
                    , (%%), (%>), (%>>), (%!>)
                    ) where

import Data.Foldable 

-- | An attribute, `name="value"`.
type Attribute = (String, String)

-- | A list of attributes.
type Attributes = [Attribute]

-- | The non-recursive basic structure of an HTML element.
data Html where
  Text :: String -> Html
  Tag :: String -> Attributes -> [Html] -> Html

-- | A new tag with a name, attributes, and body.
tag :: String -> Attributes -> [Html] -> Html
tag name attrs body = Tag name attrs body

-- | A new tag with a name, attributes, and single body element.
mkTag :: String -> Attributes -> Html -> Html
mkTag name attrs body = tag name attrs [body]

-- | A new tag with no body.
emptyTag :: String -> Attributes -> Html
emptyTag name attrs = Tag name attrs []

-- | A new empty tag.
-- | It is recommended to use this and then build up the tag with %% and %>.
newTag :: String -> Html
newTag name = emptyTag name []

-- | Text embedded into HTML.
text :: String -> Html
text = Text

infixl 4 %%
infixl 4 %>
infixl 4 %>>
infixl 4 %!>

-- | Adds an attribute to an HTML element.
(%%) :: Html -> Attribute -> Html
(Tag name attrs body) %% attr = tag name (attrs ++ [attr]) body

-- | Adds a body element to an HTML element.
(%>) :: Html -> Html -> Html
(Tag name attrs body) %> newtag = tag name attrs (body ++ [newtag])

-- | Adds body text to an HTML element.
(%>>) :: Html -> String -> Html
tag %>> txt = tag %> text txt

-- | Appends a foldable structure of Html elements to an element.
-- | This can be used for e.g. adding a list of elements, or a Maybe element.
(%!>) :: Foldable t => Html -> t Html -> Html
html %!> tags = foldl' (%>) (text "") tags

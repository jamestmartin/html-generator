module HtmlGen.Tags where

import HtmlGen.Html

html :: Html
html = newTag "html"

headTag :: Html
headTag = newTag "head"

body :: Html
body = newTag "body"

meta :: Html
meta = newTag "meta"

xmlns :: Attribute
xmlns = ("xmlns", "http://www.w3.org/1999/xhtml")

langEn :: Attribute
langEn = ("lang", "en")

utf8 :: Html
utf8 = meta %% ("charset", "utf-8")

mkMeta :: String -> String -> Html
mkMeta name content = meta %% ("name", name) %% ("content", content)

author :: String -> Html
author = mkMeta "author"

allowRobots :: Html
allowRobots = mkMeta "robots" "index,follow"

title :: Html 
title = newTag "title"

linkTag :: Html
linkTag = newTag "link"

link :: String -> String -> String -> Html
link href rel ty = linkTag %% ("href", href) %% ("rel", rel) %% ("type", ty)

stylesheet :: String -> Html
stylesheet href = link href "stylesheet" "text/css"

favicon :: String -> String -> Html
favicon href ty = link href "icon" ty

viewport :: Html
viewport = mkMeta "viewport" "with=device-width,initial-scale=1"

divTag :: Html
divTag = newTag "div"

nav :: Html
nav = newTag "nav"

spanTag :: Html
spanTag = newTag "span"

withId :: Html -> String -> Html
withId tag id = tag %% ("id", id)

withClass :: Html -> String -> Html
withClass tag clazz = tag %% ("class", clazz)

header :: Html
header = newTag "header"

article :: Html
article = newTag "article"

footer :: Html
footer = newTag "footer"

p :: Html
p = newTag "p"

hn :: Int -> Html
hn n = newTag ("h" ++ show n)

h1 :: Html
h1 = hn 1

h2 :: Html
h2 = hn 2

h3 :: Html
h3 = hn 3

h4 :: Html
h4 = hn 4

ul :: Html
ul = newTag "ul"

li :: Html
li = newTag "li"

genericList :: Html -> [Html] -> Html
genericList base items = base %!> (map (li %>) items)

unorderedList :: [Html] -> Html
unorderedList = genericList ul

href :: String -> Html -> Html
href hr body = newTag "a" %% ("href", hr) %> body

img :: String -> String -> Html
img alt src = newTag "img" %% ("src", src) %% ("alt", alt) %% ("title", alt)

bold :: String -> Html
bold txt = newTag "b" %>> txt

strong :: String -> Html
strong txt = newTag "strong" %>> txt

italic :: String -> Html
italic txt = newTag "i" %>> txt

em :: String -> Html
em txt = newTag "em" %>> txt

metaHeader :: String -> String -> Html
metaHeader header content =
  meta
  %% ("http-equiv", header)
  %% ("content", content)

blockAllMixedContent :: Html
blockAllMixedContent = metaHeader "Content-Security-Policy" "block-all-mixed-content"

metaRefresh :: String -> Html
metaRefresh url = metaHeader "refresh" ("0;url=" ++ url)

cspMeta :: String -> Html
cspMeta content = metaHeader "Content-Security-Policy" content

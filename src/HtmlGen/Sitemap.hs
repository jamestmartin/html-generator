module HtmlGen.Sitemap
  ( Sitemap, baseSitemap
  , withLastMod, withChangeFreq, withPriority
  , printSitemap
  ) where

import HtmlGen.Html
import HtmlGen.Print

data Sitemap = Sitemap
  { smLoc :: String
  , smLastMod :: Maybe String
  , smChangeFreq :: Maybe String
  , smPriority :: Maybe String
  }

-- | Takes a URL and makes a sitemap entry from it.
baseSitemap :: String -> Sitemap
baseSitemap loc = Sitemap
  { smLoc = loc
  , smLastMod = Nothing
  , smChangeFreq = Nothing
  , smPriority = Nothing
  }

-- | Sets the last modified date in the sitemap entry.
-- | The format must be YYYY-MM-DD.
withLastMod :: Sitemap -> String -> Sitemap
withLastMod sm lm = sm { smLastMod = Just lm }

-- | Sets the change frequency of the sitemap entry.
-- | See https://www.sitemaps.org/protocol.html for more info.
withChangeFreq :: Sitemap -> String -> Sitemap
withChangeFreq sm cf = sm { smChangeFreq = Just cf }

-- | Sets the priority of the sitemap entry.
-- | See https://www.sitemaps.org/protocol.html for more info.
withPriority :: Sitemap -> String -> Sitemap
withPriority sm pr = sm { smPriority = Just pr }

maybeInTag :: String -> Maybe String -> Maybe Html
maybeInTag tag thing = (newTag tag %>>) <$> thing

printUrl :: Sitemap -> Html
printUrl url =
  newTag "url"
  %> (newTag "loc" %>> smLoc url)
  %!> (maybeInTag "lastmod" $ smLastMod url)
  %!> (maybeInTag "changefreq" $ smChangeFreq url)
  %!> (maybeInTag "priority" $ smPriority url)

printUrls :: [Sitemap] -> [Html]
printUrls = map printUrl

-- | Prints a sitemap.xml from a list of Sitemap entries.
printSitemap :: [Sitemap] -> String
printSitemap urls = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" ++
  (printHtmlTree $
    (newTag "urlset" %% ("xmlns" , "http://www.sitemaps.org/schemas/sitemap/0.9"))
    %!> printUrls urls
  )

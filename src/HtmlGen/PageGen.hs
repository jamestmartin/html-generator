module HtmlGen.PageGen (genPageFromHs, genPageSitemapFromHs, genPageFromFile, genPageSitemapFromFile, genPages, genPagesSitemap) where

import HtmlGen.Html
import HtmlGen.Print
import HtmlGen.Sitemap

import Data.List (intercalate)

import Control.Monad
import Language.Haskell.Interpreter

import System.Environment (getArgs)
import System.FilePath.Posix (replaceExtension)

errorString :: InterpreterError -> String
errorString (WontCompile es) = intercalate "\n" (header : map unbox es)
  where
    header = "ERROR: Won't compile:"
    unbox (GhcError e) = e
errorString e = show e

genPageSitemapFromHsBase :: Bool -> FilePath -> Interpreter (String, Maybe Sitemap)
genPageSitemapFromHsBase makeSitemap file = do
  loadModules [file]
  setTopLevelModules ["Page"]
  html <- interpret "page" (as :: Html)
  sitemap <- if makeSitemap
    then Just <$> interpret "sitemap" (as :: Sitemap)
    else return Nothing
  return (printHtml html, sitemap)

genPageFromHs :: FilePath -> Interpreter String
genPageFromHs file = do
  (page, _) <- genPageSitemapFromHsBase False file
  return page

genPageSitemapFromHs :: FilePath -> Interpreter (String, Sitemap)
genPageSitemapFromHs file = do
  (page, Just sitemap) <- genPageSitemapFromHsBase True file
  return (page, sitemap)

genPageSitemapFromFileBase :: Bool -> FilePath -> IO (Maybe Sitemap)
genPageSitemapFromFileBase makeSitemap file = do
  expr <- runInterpreter $ genPageSitemapFromHsBase makeSitemap file
  case expr of
    Left err -> putStrLn (errorString err ++ " in page " ++ file) >> return Nothing
    Right (generatedHtml, sitemap) ->
      writeFile (replaceExtension file "xhtml") generatedHtml >> return sitemap

genPageFromFile :: FilePath -> IO ()
genPageFromFile file = genPageSitemapFromFileBase False file >> return ()

genPageSitemapFromFile :: FilePath -> IO Sitemap
genPageSitemapFromFile file = do
  Just sitemap <- genPageSitemapFromFileBase True file
  return sitemap

genPages :: [FilePath] -> IO ()
genPages files = sequence_ $ map genPageFromFile files

genPagesSitemap :: [FilePath] -> IO ()
genPagesSitemap files = do
  sitemap <- sequence $ map genPageSitemapFromFile files
  writeFile "sitemap.xml" $ printSitemap sitemap
